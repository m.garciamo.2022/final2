from transforms import *
import sys
from images import *

## Puede haber una o más funciones definidas, que utilizará main()

def parametros():
    nombre = sys.argv[1]
    trans = sys.argv[2]
    return nombre, trans

def main():
    fichero, trans = parametros()
    image = read_img(f"{fichero}")
    img = None

    if trans == "mirror":
        img = mirror(image)

    elif trans == "blur":
        img = blur(image)

    elif trans == "grayscale":
        img = grayscale(image)

    elif trans == "sepia":
        img = sepia(image)

    elif trans == "negative":
        img = negative(image)

    elif trans == "change_colors":
        parameter1 = sys.argv[3].split(":")
        parameter2 = sys.argv[4].split(":")
        original = []
        change = []
        for i in range(len(parameter1)):
            r, g, b = parameter1[i].split(",")
            original.append((int(r), int(g), int(b)))
        for i in range(len(parameter2)):
            r, g, b = parameter2[i].split(",")
            change.append((int(r), int(g), int(b)))
        img = change_colors(image, original, change)

    elif trans == "rotate":
        parameter1 = sys.argv[3]
        img = rotate(image, parameter1)

    elif trans == "shift":
        parameter1 = int(sys.argv[3])
        parameter2 = int(sys.argv[4])
        img = shift(image, parameter1, parameter2)

    elif trans == "crop":
        parameter1 = int(sys.argv[3])
        parameter2 = int(sys.argv[4])
        parameter3 = int(sys.argv[5])
        parameter4 = int(sys.argv[6])
        img = crop(image, parameter1, parameter2, parameter3, parameter4)

    elif trans == "filter":
        parameter1 = float(sys.argv[3])
        parameter2 = float(sys.argv[4])
        parameter3 = float(sys.argv[5])
        img = filter(image, parameter1, parameter2, parameter3)

    elif trans == "luminity":
        parameter1 = int(sys.argv[3])
        img = luminity(image, parameter1)

    else:
        print('Introduzca una de las funciones sugeridas: \n'
              '- mirror\n'
              '- blur\n'
              '- grayscale\n'
              '- sepia\n'
              '- negative\n'
              '- change_colors\n'
              '- rotate\n'
              '- shift\n'
              '- crop\n'
              '- filter\n'
              '- luminity')
        return

    if img:
        nombre, extension = sys.argv[1].split("/")[-1].split(".")
        write_img(img, f"{fichero}_trans.{extension}")
        Image.open(f"{fichero}_trans.{extension}").show()

if __name__ == '__main__':
    main()
