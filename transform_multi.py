from transforms import *
import sys
from images import *

## Puede haber una o más funciones definidas, que utilizará main()

def parametros():
    nombre = sys.argv[1]
    return nombre

def main():
    fichero = parametros()
    img = read_img(f"{fichero}")
    contador = 2

    while True:
        trans = sys.argv[contador]

        if trans == "mirror":
            img = mirror(img)
            contador += 1

        elif trans == "blur":
            img = blur(img)
            contador += 1

        elif trans == "grayscale":
            img = grayscale(img)
            contador += 1

        elif trans == "sepia":
            img = grayscale(img)
            contador += 1

        elif trans == "negative":
            img = negative(img)
            contador += 1

        elif trans == "change_colors":
            parameter1 = sys.argv[contador+1].split(":")
            parameter2 = sys.argv[contador+2].split(":")
            original = []
            change = []
            contador += 3

            for i in range(len(parameter1)):
                r, g, b = parameter1[i].split(",")
                original.append((int(r), int(g), int(b)))

            for i in range(len(parameter2)):
                r, g, b = parameter2[i].split(",")
                change.append((int(r), int(g), int(b)))

            if len(original) != len(change):
                exit("\nDebe haber el mismo numero de colores a cambiar que por los que van a ser cambiados.\n")

            img = change_colors(img, original, change)

        elif trans == "rotate":
            parameter1 = sys.argv[contador+1]
            contador += 2
            img = rotate(img, parameter1)

        elif trans == "shift":
            parameter1 = int(sys.argv[contador+1])
            parameter2 = int(sys.argv[contador+2])
            contador += 3
            img = shift(img, parameter1, parameter2)

        elif trans == "crop":
            parameter1 = int(sys.argv[contador+1])
            parameter2 = int(sys.argv[contador+2])
            parameter3 = int(sys.argv[contador+3])
            parameter4 = int(sys.argv[contador+4])
            contador += 5
            img = crop(img, parameter1, parameter2, parameter3, parameter4)

        elif trans == "filter":
            parameter1 = float(sys.argv[contador+1])
            parameter2 = float(sys.argv[contador+2])
            parameter3 = float(sys.argv[contador+3])
            contador += 4
            img = filter(img, parameter1, parameter2, parameter3)

        elif trans == "luminity":
            parameter1 = int(sys.argv[contador+1])
            contador += 2
            img = luminity(img, parameter1)

        if contador == len(sys.argv):
            break

    if img:
        nombre, extension = sys.argv[1].split("/")[-1].split(".")
        write_img(img, f"{fichero}_trans.{extension}")
        Image.open(f"{fichero}_trans.{extension}").show()

if __name__ == '__main__':
    main()
