from transforms import *
import sys
from images import *

## Puede haber una o más funciones definidas, que utilizará main()

def parametros():
    nombre = sys.argv[1]
    trans = sys.argv[2]
    return nombre, trans

def main():
    fichero, trans = parametros()
    image = read_img(f"{fichero}")
    img = None

    if trans == "mirror":
        img = mirror(image)

    elif trans == "blur":
        img = blur(image)

    elif trans == "grayscale":
        img = grayscale(image)

    elif trans == "sepia":
        img = sepia(image)

    elif trans == "negative":
        img = negative(image)

    else:
        print('Introduzca una de las funciones sugeridas: \n'
              '- mirror\n'
              '- blur\n'
              '- grayscale\n'
              '- sepia\n'
              '- negative')
        return

    if img:
        nombre, extension = sys.argv[1].split("/")[-1].split(".")
        write_img(img, f"{nombre}_trans.{extension}")
        Image.open(f"{nombre}_trans.{extension}").show()

if __name__ == '__main__':
    main()
