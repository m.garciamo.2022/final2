from images import *

def mirror(image: dict) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for pixel in range(0, width*height):
        img["pixels"][pixel%width + (width * (height - ((pixel//width) + 1)))] = image["pixels"][pixel]
    return img

def grayscale(image: dict) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for pixel in range(0, width*height):
        r, g, b = image["pixels"][pixel]
        gray = (r + g + b)//3
        img["pixels"][pixel] = (gray, gray, gray)
    return img

def blur(image: dict) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    imagen = image["pixels"]
    for pixel in range(0, width*height):
        r, g, b = 0, 0, 0
        n = 0
        if pixel%width !=0:
            r, g, b = r + imagen[pixel-1][0], g + imagen[pixel-1][1], b + imagen[pixel-1][2]
            n += 1
        if pixel%width != width-1:
            r, g, b = r + imagen[pixel+1][0], g + imagen[pixel+1][1], b + imagen[pixel+1][2]
            n += 1
        if pixel//width != 0:
            r, g, b = r + imagen[pixel-width][0], g + imagen[pixel-width][1], b + imagen[pixel-width][2]
            n += 1
        if pixel//width != height-1:
            r, g, b = r + imagen[pixel+width][0], g + imagen[pixel+width][1], b + imagen[pixel+width][2]
            n += 1
        r, g, b = r//n, g//n, b//n
        img["pixels"][pixel] = (r, g, b)
    return img

def change_colors(image: dict, original: list[tuple[int, int, int]], change: list[tuple[int, int, int]]) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for pixel in range(0, width*height):
        img["pixels"][pixel] = image["pixels"][pixel]
        for color in range(len(original)):
            if image["pixels"][pixel] == original[color]:
                img["pixels"][pixel] = change[color]
                break
    return img

def rotate(image: dict, direction: str) -> dict:
    width, height = size(image)
    img = create_blank(height, width)
    for pixel in range(0, width*height):
        if direction == "right":
            img["pixels"][height-((pixel//width)+1) + height*(pixel%width)] = image["pixels"][pixel]
        if direction == "left":
            img["pixels"][(pixel//width) + height * (width-((pixel%width)+1))] = image["pixels"][pixel]
    return img

def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    width, height = size(image)
    img = create_blank(width + horizontal, height + vertical)
    for pixel in range (0, width*height):
        img["pixels"][(pixel%width) + horizontal + (width+horizontal) * ((pixel//width)+vertical)] = image["pixels"][pixel]
    return img

def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
    width1, height1 = size(image)
    img = create_blank(width, height)
    for pixel in range(0, width*height):
        img["pixels"][pixel] = image["pixels"][(pixel%width) + x + width1*((pixel//width) + y)]
    return img

def filter(image: dict, r: float, g: float, b: float) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for pixel in range(0, width*height):
        r1, g1, b1 = int(image["pixels"][pixel][0] * r), int(image["pixels"][pixel][1] * g), int(image["pixels"][pixel][2] * b)
        if r1 > 255:
            r1 = 255
        if g1 > 255:
            g1 = 255
        if b1 > 255:
            b1 = 255
        img["pixels"][pixel] = (r1, g1, b1)
    return img

def sepia(image):
    width, height = size(image)
    img = create_blank(width, height)
    for pixel in range(0, width*height):
        r, g, b = image["pixels"][pixel]
        r, g, b = 0.393*r+0.769*g+0.189*b, 0.349*r+0.686*g+0.168*b, 0.272*r+0.534*g+0.131*b
        if r > 255:
            r = 255
        if g > 255:
            g = 255
        if b > 255:
            b = 255
        img["pixels"][pixel] = int(r), int(g), int(b)
    return img

def luminity(image: dict, indice: int) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for pixel in range(0, width*height):
        r, g, b = image["pixels"][pixel]
        r, g, b = r+indice, g+indice, b+indice
        if r > 255:
            r = 255
        if g > 255:
            g = 255
        if b > 255:
            b = 255
        if r < 0:
            r = 0
        if g < 0:
            g = 0
        if b < 0:
            b = 0
        img["pixels"][pixel] = (r, g, b)
    return img

def negative(image: dict) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for pixel in range(0, width*height):
        r, g, b = image["pixels"][pixel]
        r, g, b = 255-r, 255-g, 255-b
        img["pixels"][pixel] = (r, g, b)
    return img
